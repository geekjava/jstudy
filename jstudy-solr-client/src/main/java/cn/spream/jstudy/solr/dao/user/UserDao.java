package cn.spream.jstudy.solr.dao.user;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import cn.spream.jstudy.solr.domain.user.User;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-3-20
 * Time: 下午3:22
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {

    public void add(User user) throws IOException, SolrServerException;

    public void deleteById(long id) throws IOException, SolrServerException;

    public void update(User user) throws IOException, SolrServerException;

    public User getById(long id) throws SolrServerException;

    public List<User> find() throws SolrServerException;

}
