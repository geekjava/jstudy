package cn.spream.jstudy.file;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Test;

import java.io.File;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-14
 * Time: 下午12:55
 * To change this template use File | Settings | File Templates.
 * 具体的测试信息可参考：
 * http://commons.apache.org/proper/commons-io//xref-test/org/apache/commons/io/FileUtilsTestCase.html
 */
public class TestFileUtils {

    /**
     * 测试输出文件夹下单所有文件，包括子文件夹下的文件
     */
    @Test
    public void testIterateFiles(){
        File file = new File("C:\\home\\test\\logs_api\\");
        Iterator<File> fileIterator = FileUtils.iterateFiles(file, new WildcardFileFilter("*.*"), new WildcardFileFilter("*"));
        while (fileIterator.hasNext()){
            File result = fileIterator.next();
            System.out.println(result.getPath());
        }
    }

}
