package cn.spream.jstudy.ssi.service.user;

import cn.spream.jstudy.ssi.common.Result;
import cn.spream.jstudy.ssi.domain.user.UserQuery;
import cn.spream.jstudy.ssi.domain.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-20
 * Time: 下午1:11
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {

    public boolean add(User user);

    public boolean update(User user);

    public User getById(long id);

    public Result list(UserQuery userQuery, int pageIndex, int pageSize);

}
