package cn.spream.jstudy.ssi.manager.user;

import cn.spream.jstudy.ssi.domain.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-20
 * Time: 下午1:00
 * To change this template use File | Settings | File Templates.
 */
public interface UserManager {

    public boolean update(User user);

}
