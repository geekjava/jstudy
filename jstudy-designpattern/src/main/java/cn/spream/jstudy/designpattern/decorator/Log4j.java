package cn.spream.jstudy.designpattern.decorator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:32
 * To change this template use File | Settings | File Templates.
 */
public class Log4j implements Log {

    @Override
    public void debug(String debug) {
        System.out.println("Log4j.debug：" + debug);
    }

    @Override
    public void info(String info) {
        System.out.println("Log4j.info：" + info);
    }

    @Override
    public void error(String error) {
        System.out.println("Log4j.error：" + error);
    }
}
