package cn.spream.jstudy.designpattern.iterator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 上午10:51
 * To change this template use File | Settings | File Templates.
 */
public interface Iterator {

    public Object previous();

    public Object next();

    public boolean hasNext();

    public Object first();

}
