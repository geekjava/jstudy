package cn.spream.jstudy.designpattern.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:35
 * To change this template use File | Settings | File Templates.
 */
public class ColleagueB extends Colleague {

    @Override
    public void setNumber(int number, Mediator mediator) {
        this.number = number;
        mediator.affectA();
    }

}
