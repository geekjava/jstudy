package cn.spream.jstudy.designpattern.iterator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 上午10:56
 * To change this template use File | Settings | File Templates.
 */
public class MyIterator implements Iterator {

    private Collection collection;
    private int pos = -1;

    public MyIterator(Collection collection) {
        this.collection = collection;
    }

    @Override
    public Object previous() {
        if (pos > 0) {
            pos -= 1;
        }
        return collection.get(pos);
    }

    @Override
    public Object next() {
        if (pos < collection.size()) {
            pos += 1;
        }
        return collection.get(pos);
    }

    @Override
    public boolean hasNext() {
        return pos < collection.size() - 1;
    }

    @Override
    public Object first() {
        pos = 0;
        return collection.get(pos);
    }

}
