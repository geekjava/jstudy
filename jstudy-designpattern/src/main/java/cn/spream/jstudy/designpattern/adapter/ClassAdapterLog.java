package cn.spream.jstudy.designpattern.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:34
 * To change this template use File | Settings | File Templates.
 */
public class ClassAdapterLog extends Log4j implements Log {

    @Override
    public void debug(String debug) {
        System.out.println("ClassAdapterLog.debug：" + debug);
    }

    @Override
    public void warn(String warn) {
        System.out.println("ClassAdapterLog.warn：" + warn);
    }

    @Override
    public void fatal(String fatal) {
        System.out.println("ClassAdapterLog.fatal：" + fatal);
    }

}
