package cn.spream.jstudy.designpattern.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:53
 * To change this template use File | Settings | File Templates.
 */
public interface Expression {

    public int interpret(Context context);

}
