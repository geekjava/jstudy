package cn.spream.jstudy.designpattern.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:27
 * To change this template use File | Settings | File Templates.
 */
public class SedanCar implements Car {

    @Override
    public void run() {
        System.out.println("小轿车开动了");
    }

}
