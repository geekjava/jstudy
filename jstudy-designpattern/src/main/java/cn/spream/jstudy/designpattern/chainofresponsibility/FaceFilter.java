package cn.spream.jstudy.designpattern.chainofresponsibility;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午2:08
 * To change this template use File | Settings | File Templates.
 */
public class FaceFilter implements Filter {

    @Override
    public void doFilter(Request request, Response response, FilterChain chain) {
        String requestContent = request.getContent();
        request.setContent(requestContent.replace(":)", "^V^"));
        chain.doFilter(request, response, chain);
        String responseContent = response.getContent();
        response.setContent(responseContent.replace("^V^", ":)"));
    }

}
