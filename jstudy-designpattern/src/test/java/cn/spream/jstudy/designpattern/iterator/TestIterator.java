package cn.spream.jstudy.designpattern.iterator;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 上午11:01
 * To change this template use File | Settings | File Templates.
 */
public class TestIterator {

    @Test
    public void test() {
        Collection collection = new MyCollection();
        collection.add("a");
        collection.add("b");
        collection.add("c");
        collection.add("d");
        collection.add("e");
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println(iterator.previous());
        System.out.println(iterator.first());
        System.out.println(iterator.hasNext());
        System.out.println(iterator.next());
    }

}
