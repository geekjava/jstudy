package cn.spream.jstudy.designpattern.decorator;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午11:14
 * To change this template use File | Settings | File Templates.
 */
public class TestDecorator {

    @Test
    public void test(){
        DecoratorLog4j decoratorLog4j = new DecoratorLog4j(new Log4j());
        decoratorLog4j.debug("decorator");
        decoratorLog4j.info("decorator");
        decoratorLog4j.error("decorator");
    }

}
