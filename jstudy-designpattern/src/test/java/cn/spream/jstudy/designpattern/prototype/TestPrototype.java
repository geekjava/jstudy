package cn.spream.jstudy.designpattern.prototype;

import org.junit.Test;

import java.io.IOException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-7
 * Time: 下午4:26
 * To change this template use File | Settings | File Templates.
 */
public class TestPrototype {

    @Test
    public void test() throws CloneNotSupportedException, IOException, ClassNotFoundException {
        Prototype prototype = new Prototype();
        User user = new User();
        prototype.setUser(user);
        Prototype clonePrototype = prototype.clone();
        Prototype deepClonePrototype = prototype.deepClone();
        System.out.println("原始数据：" + prototype);
        long id = new Date().getTime();
        user.setId(id);
        System.out.println("修改数据：user.id=" + id);
        System.out.println("浅 拷 贝：" + clonePrototype);
        System.out.println("深 拷 贝：" + deepClonePrototype);
    }

}
