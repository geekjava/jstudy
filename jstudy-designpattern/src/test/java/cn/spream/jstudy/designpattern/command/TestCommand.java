package cn.spream.jstudy.designpattern.command;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午2:57
 * To change this template use File | Settings | File Templates.
 */
public class TestCommand {

    @Test
    public void test(){
        Receiver receiver = new Receiver();
        Command command = new ConcreteCommand(receiver);
        Invoker invoker = new Invoker(command);
        invoker.action();
    }

}
