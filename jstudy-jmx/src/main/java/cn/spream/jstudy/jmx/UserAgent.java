package cn.spream.jstudy.jmx;

import com.sun.jdmk.comm.HtmlAdaptorServer;

import javax.management.*;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-4-14
 * Time: 上午11:32
 * To change this template use File | Settings | File Templates.
 */
public class UserAgent {

    private static String jmxServerName = "UserJMXServer";

    public static void main(String[] args) throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        MBeanServer mBeanServer = MBeanServerFactory.createMBeanServer();

        ObjectName userObjectName = new ObjectName(jmxServerName + ":name=user");
        mBeanServer.registerMBean(new User(), userObjectName);

        //需要加入jmxtools.jar包，添加到java的classpath中或者添加到maven中
        HtmlAdaptorServer htmlAdaptorServer = new HtmlAdaptorServer(6060);
        ObjectName htmlAdapterObjectName = new ObjectName(jmxServerName + ":name=htmlAdapterObjectName");
        mBeanServer.registerMBean(htmlAdaptorServer, htmlAdapterObjectName);
        //启动并访问127.0.0.1:6060端口
        htmlAdaptorServer.start();
    }

}
