package cn.spream.jstudy.mongodb.dao.user;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import cn.spream.jstudy.mongodb.domain.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: cn.spream
 * Date: 12-12-3
 * Time: 上午11:35
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDao {

    private UserDao userDaoMongoDB;
    private UserDao userDaoMongoTemplate;

    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        userDaoMongoDB = (UserDao) context.getBean("userDaoMongoDB");
        userDaoMongoTemplate = (UserDao) context.getBean("userDaoMongoDB");
    }

    @Test
    public void testAdd() {
        for (int i = 0; i < 10000; i++) {
            User user = new User();
            user.setName("张三");
            user.setSex(User.Sex.WOMAN.getKey());
            user.setAge(25);
            user.setMobile("18210599999");
            user.setAddress("北京");
//            userDaoMongoDB.add(user);
            userDaoMongoTemplate.add(user);
        }
    }

}
