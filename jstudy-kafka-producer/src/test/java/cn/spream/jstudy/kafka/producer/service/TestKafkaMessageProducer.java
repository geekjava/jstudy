package cn.spream.jstudy.kafka.producer.service;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-8-27
 * Time: 上午9:50
 * To change this template use File | Settings | File Templates.
 */
public class TestKafkaMessageProducer {

    private KafkaMessageProducer kafkaMessageProducer;

    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-producer.xml");
        kafkaMessageProducer = (KafkaMessageProducer) context.getBean("kafkaMessageProducer");
    }

    @Test
    public void testSend() throws ExecutionException, InterruptedException {
        String message = "Hello World! " + UUID.randomUUID().toString();
        System.out.println("message:" + message);
        Future<RecordMetadata> future = kafkaMessageProducer.send(message, new Callback() {
            @Override
            public void onCompletion(RecordMetadata recordMetadata, Exception exception) {
                if(exception != null){
                    exception.printStackTrace();
                }else{
                    System.out.println("onCompletion, topic:" + recordMetadata.topic() + ", partition:" + recordMetadata.partition() + ", offset:" + recordMetadata.offset());
                }
            }
        });
        RecordMetadata recordMetadata = future.get();
        System.out.println("end, topic:" + recordMetadata.topic() + ", partition:" + recordMetadata.partition() + ", offset:" + recordMetadata.offset());
    }

    @Test
    public void testAutoSend() throws ExecutionException, InterruptedException {
        while(true){
            testSend();
            TimeUnit.SECONDS.sleep(3);
        }
    }

}
